import Helper from './helper';

describe('Helper', () => {

  it('should be empty string if page number equals 1', () => {
    expect(Helper.getPageNumberQueryString(1)).toEqual('');
  });
  it('should be "/?page=3" if page number equals 3', () => {
    expect(Helper.getPageNumberQueryString(3)).toEqual('/?page=3');
  });
  it('should be 1 if number of item to fetch equals 10', () => {
    expect(Helper.getNumberOfPagesToFetch(10)).toEqual(1);
  });
  it('should be 2 if number of item to fetch equals 40', () => {
    expect(Helper.getNumberOfPagesToFetch(40)).toEqual(4);
  });
  it('should be 3 if number of item to fetch equals 25', () => {
    expect(Helper.getNumberOfPagesToFetch(25)).toEqual(3);
  });
  it('should be true if number of downloaded items equals all available planets', () => {
    expect(Helper.isAllPlanetsDownloaded(61, 61)).toBeTruthy();
  });
  it('should be false if number of downloaded items are less then all available planets', () => {
    expect(Helper.isAllPlanetsDownloaded(30, 61)).toBeFalsy();
  });

});
