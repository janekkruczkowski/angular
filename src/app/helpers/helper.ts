export default class Helper {
  public static getNumberOfPagesToFetch(numberOfItems: number): number {
    if (numberOfItems % 10 === 0) {
      return Math.floor(numberOfItems / 10);
    }
    return Math.floor(numberOfItems / 10) + 1;
  }

  public static getPageNumberQueryString(pageNumber: number): string {
    if (pageNumber !== 1) {
      return `/?page=${pageNumber}`;
    }
    return '';
  }

  public static calculatePageNumberAfterItemsPerPageChanged(
    newNumberOfItems: number,
    numberOfPlanetsPerPage: number,
    currentPage: number
  ): number {
    const startIndex = numberOfPlanetsPerPage * currentPage;
    const pageNumber = startIndex / newNumberOfItems;
    if (pageNumber < 1) {
      return 1;
    }
    return Math.floor(pageNumber);
  }

  public static isAllPlanetsDownloaded(
    downloadedItemsCount: number,
    allAvailablePlanets: number
  ): boolean {
    return downloadedItemsCount === allAvailablePlanets;
  }
}
