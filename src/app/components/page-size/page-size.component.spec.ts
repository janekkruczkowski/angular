import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageSizeComponent } from './page-size.component';
import {FormsModule} from '@angular/forms';

describe('PageSizeComponent', () => {
  let component: PageSizeComponent;
  let fixture: ComponentFixture<PageSizeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule],
      declarations: [ PageSizeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageSizeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should contain 5, 10, 25 and 100 inside sizes array', () => {
    expect(component.sizes).toContain(5);
    expect(component.sizes).toContain(10);
    expect(component.sizes).toContain(25);
    expect(component.sizes).toContain(100);
  });
});
