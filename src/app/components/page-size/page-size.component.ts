import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-page-size',
  templateUrl: './page-size.component.html',
  styleUrls: ['./page-size.component.css']
})
export class PageSizeComponent implements OnInit {

  @Input() public planetsPerPage = 5;
  @Input() public disabled = false;
  @Output() public valueChanged = new EventEmitter<number>();
  public sizes: number[] = [5, 10, 25, 100];

  constructor() {
  }

  ngOnInit() {
  }

  selectValueChanged($event: any) {
    this.valueChanged.emit($event.target.value);
  }

}
