import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomepageComponent } from './homepage.component';
import {FormsModule} from '@angular/forms';
import {RouterTestingModule} from '@angular/router/testing';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {PageSizeComponent} from '../../components/page-size/page-size.component';
import {ApiService} from '../../services/api.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {LoadingComponent} from '../../components/loading/loading.component';
import {ErrorComponent} from '../../components/message/error.component';


describe('HomepageComponent', () => {
  let component: HomepageComponent;
  let fixture: ComponentFixture<HomepageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule, RouterTestingModule, NgbModule, HttpClientTestingModule],
      declarations: [HomepageComponent, PageSizeComponent, LoadingComponent, ErrorComponent],
      providers: [ApiService]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomepageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should render "List of planets" in a h1 tag', async(() => {
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('h1').textContent).toContain('List of planets');
  }));
  it('should render "search" as placeholder', async(() => {
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('input').placeholder).toContain('Search');
  }));
  it('should render Loading if data is null', async(() => {
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('input').placeholder).toContain('Search');
  }));

});
