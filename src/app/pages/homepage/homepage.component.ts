import {ApiService} from '../../services/api.service';
import {Component, OnDestroy, OnInit} from '@angular/core';
import {Planet} from '../../models/planet.interface';
import {DataService} from '../../services/data.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Subscription} from 'rxjs';
import {filter} from 'rxjs/operators';
import Helper from '../../helpers/helper';
import {fakeAsync} from "@angular/core/testing";

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.css']
})
export class HomepageComponent implements OnInit, OnDestroy {
  private planets: Planet[];
  private page = 1;
  private planetsPerPage: number;
  private planetsCount = 0;
  private errorMessage: string;
  private searchInput: string;
  private isLoading = true;
  private isSearching: boolean;
  private isError: boolean;
  private isNewSearch: boolean;
  private dataSubscription$: Subscription;
  private routerParamsSubscription$: Subscription;

  constructor(
    private api: ApiService,
    private data: DataService,
    private router: Router,
    private route: ActivatedRoute
  ) {
  }

  ngOnInit(): void {
    this.planetsPerPage = this.data.planetsPerPage;
    this.setupDataSubscription();
    this.setupRouterParamsSubscription();
  }

  ngOnDestroy(): void {
    this.dataSubscription$.unsubscribe();
    this.routerParamsSubscription$.unsubscribe();
  }

  private setupDataSubscription() {
    this.dataSubscription$ = this.data.planetSubject
      .pipe(filter(res => res.planets.length > 0 || res.error !== null))
      .subscribe(
        res => {
          this.planetsCount = res.totalItemsCount;
          this.planets = res.planets;
          this.isLoading = false;
          this.setError(res.error);
        },
        () => (this.isError = true)
      );
  }

  private setupRouterParamsSubscription() {
    this.routerParamsSubscription$ = this.route.params.subscribe(param => {
      const query = this.route.snapshot.queryParamMap.get('q');
      this.isLoading = true;
      const page = param.number;
      if (query) {
        this.page = page;
        this.isNewSearch = false;
        this.searchInput = query;
        this.search(query);
        return;
      }
      if (page > 0) {
        this.loadDataForSpecificPage(page);
        this.page = page;
        return;
      }
      this.data.getData(this.planetsPerPage);
    });
  }

  private setError(error: string) {
    if (error) {
      this.isError = true;
      this.errorMessage = error;
    } else {
      this.isError = false;
      this.errorMessage = null;
    }
  }

  private itemsPerPageChanged(numberOfItems: number): void {
    const newPage = Helper.calculatePageNumberAfterItemsPerPageChanged(
      numberOfItems,
      this.planetsPerPage,
      this.page
    );
    if (newPage !== this.page) {
      this.router.navigate(['page', newPage]);
    }
    this.planetsPerPage = numberOfItems;
    this.data.planetsPerPage = numberOfItems;
    this.isLoading = true;
    const items = newPage * this.planetsPerPage;
    if (this.searchInput && this.searchInput.trim() !== '') {
      this.page = newPage;
      if (!this.route.snapshot.queryParamMap.get('q')) {
        this.router.navigate(['/page/1'], {
          queryParams: {q: this.searchInput}
        });
      }
      this.search(this.searchInput);
      return;
    }
    this.data.getData(items, items - this.planetsPerPage);
  }

  private search(searchTerm: string): void {
    if (this.isNewSearch !== false) {
      this.isNewSearch = true;
      this.page = 1;
    }
    const items = this.page * this.planetsPerPage;
    const startIndex = items - this.planetsPerPage;
    if (searchTerm.length === 0) {
      this.isSearching = false;
      this.isNewSearch = false;
      this.data.getData(items, startIndex);
      this.router.navigate(['/']);
      return;
    }
    this.isSearching = true;
    if (!this.data.isAllPlanetsDownloaded()) {
      this.isLoading = true;
    }
    this.data.search(searchTerm, items, startIndex);
  }

  private loadDataForSpecificPage(pageNumber: number): void {
    const items = pageNumber * this.planetsPerPage;
    this.isLoading = true;
    if (this.searchInput && this.searchInput.trim() !== '') {
      this.search(this.searchInput);
      return;
    }
    this.data.getData(items, items - this.planetsPerPage);
  }

  private pageChanged(newNumberOfPage: number): void {
    this.router.navigate(['page', newNumberOfPage], {
      queryParams: {q: this.searchInput}
    });
  }

  private loadDetails(planetURL: string) {
    const planetId = planetURL.match(/[0-9]{1,2}/);
    if (planetId.length === 1 && parseInt(planetId[0], 0)) {
      this.router.navigate(['/planet', planetId[0]]);
    }
  }
}
