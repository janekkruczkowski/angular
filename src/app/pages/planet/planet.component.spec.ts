import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlanetComponent } from './planet.component';
import {RouterTestingModule} from '@angular/router/testing';
import {ApiService} from '../../services/api.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {ErrorComponent} from '../../components/message/error.component';
import {LoadingComponent} from '../../components/loading/loading.component';

describe('PlanetComponent', () => {
  let component: PlanetComponent;
  let fixture: ComponentFixture<PlanetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, HttpClientTestingModule],
      declarations: [PlanetComponent, ErrorComponent, LoadingComponent],
      providers: [ApiService]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlanetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });


});
