import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { DataService } from '../../services/data.service';
import { Planet } from '../../models/planet.interface';
import { PlanetSubject } from '../../models/planet.subject.interface';

@Component({
  selector: 'app-planet',
  templateUrl: './planet.component.html',
  styleUrls: ['./planet.component.css']
})
export class PlanetComponent implements OnInit {
  planet: Planet;
  error: string;

  constructor(
    private route: ActivatedRoute,
    private data: DataService,
    private location: Location,
    private router: Router
  ) {}

  ngOnInit(): void {
    const planet: PlanetSubject = this.route.snapshot.data.planet;
    if (!planet) {
      this.router.navigate(['/']);
      return;
    }
    if (planet.error) {
      this.error = planet.error;
      return;
    }
    this.planet = planet.planets[0];
  }

  goBack(): void {
    if (this.data.planets.length === 0) {
      this.router.navigate(['/']);
      return;
    }
    this.location.back();
  }
}
