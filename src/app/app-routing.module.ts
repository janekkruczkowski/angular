import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PlanetComponent } from './pages/planet/planet.component';
import { HomepageComponent } from './pages/homepage/homepage.component';
import { PlanetResolver } from './services/planet-resolver';
import { HttpClientModule } from '@angular/common/http';
import { ApiService } from './services/api.service';
import { NotFoundComponent } from './pages/not-found/not-found.component';

const routes: Routes = [
  { path: '', component: HomepageComponent },
  { path: 'page/:number', component: HomepageComponent },
  {
    path: 'planet/:id',
    component: PlanetComponent,
    resolve: { planet: PlanetResolver }
  },
  { path: '**', redirectTo: '404' },
  { path: '404', component: NotFoundComponent }
];

@NgModule({
  imports: [HttpClientModule, RouterModule.forRoot(routes)],
  providers: [ApiService],
  exports: [RouterModule]
})
export class AppRoutingModule {}
