import { Planet } from './planet.interface';

export interface PlanetsResponse {
  count: number;
  results: Planet[];
}
