import { Planet } from './planet.interface';

export interface PlanetSubject {
  planets: Planet[];
  totalItemsCount: number;
  error?: string;
}
