import { Injectable } from '@angular/core';

import {
  ActivatedRouteSnapshot,
  Resolve,
  RouterStateSnapshot
} from '@angular/router';

import { Observable, of } from 'rxjs';
import { DataService } from './data.service';
import { catchError } from 'rxjs/operators';
import { PlanetSubject } from '../models/planet.subject.interface';

@Injectable()
export class PlanetResolver implements Resolve<Observable<PlanetSubject>> {
  constructor(private data: DataService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const id = parseInt(route.paramMap.get('id'), 0);
    if (isNaN(id)) {
      const planetData: PlanetSubject = {
        planets: [],
        totalItemsCount: 0,
        error: 'Bad request'
      };
      return of(planetData);
    }
    return this.data.getSinglePlanet(id).pipe(
      catchError(err => {
        const planetData: PlanetSubject = {
          planets: [],
          totalItemsCount: 0,
          error: err
        };
        return of(planetData);
      })
    );
  }
}
