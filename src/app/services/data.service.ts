import { Injectable } from '@angular/core';
import { Planet } from '../models/planet.interface';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { map, take } from 'rxjs/operators';
import { ApiService } from './api.service';
import Helper from '../helpers/helper';
import { PlanetSubject } from '../models/planet.subject.interface';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  planets: Planet[] = [];
  count: number;
  planetSubject: BehaviorSubject<PlanetSubject> = new BehaviorSubject<
    PlanetSubject
  >({ planets: this.planets, totalItemsCount: this.count });
  planetsPerPage = 5;

  constructor(private api: ApiService) {}

  public getData(numberOfItemsToGet: number, indexOfFirstItemToGet: number = 0): void {
    this.count = this.api.totalPlanetsCount;
    if (
      numberOfItemsToGet > this.planets.length &&
      (!this.api.totalPlanetsCount ||
        (this.api.totalPlanetsCount && !this.isAllPlanetsDownloaded()))
    ) {
      this.downloadPlanets(
        numberOfItemsToGet,
        indexOfFirstItemToGet,
        this.planets.length
      );
    } else {
      if (numberOfItemsToGet > this.planets.length) {
        this.pushData(
          this.planets.slice(indexOfFirstItemToGet, this.planets.length)
        );
      } else {
        this.pushData(
          this.planets.slice(indexOfFirstItemToGet, numberOfItemsToGet)
        );
      }
    }
  }

  public getSinglePlanet(planetId: number): Observable<PlanetSubject> {
    const filteredPLancet = this.planets.filter(
      val => val.url.indexOf(`https://swapi.co/api/planets/${planetId}/`) !== -1
    );
    if (filteredPLancet.length === 1) {
      const planetData: PlanetSubject = {
        planets: filteredPLancet,
        totalItemsCount: 1
      };
      return of(planetData);
    }
    return this.api.fetchSinglePlanet(planetId).pipe(
      map(res => {
        const planetData: PlanetSubject = {
          planets: [res],
          totalItemsCount: 1
        };
        return planetData;
      })
    );
  }

  public search(
    term: string,
    numberOfItemsToGet: number,
    indexOfFirstItemToGet: number
  ): void {
    if (this.isAllPlanetsDownloaded()) {
      this.searchPlanets(term, numberOfItemsToGet, indexOfFirstItemToGet);
      return;
    }
    if (!this.api.totalPlanetsCount) {
      this.downloadPlanets(5, 0, this.planets.length, () =>
        this.search(term, numberOfItemsToGet, indexOfFirstItemToGet)
      );
      return;
    }
    this.downloadPlanets(
      this.api.totalPlanetsCount,
      0,
      this.planets.length,
      () => this.searchPlanets(term, numberOfItemsToGet, indexOfFirstItemToGet)
    );
  }

  public isAllPlanetsDownloaded() {
    return Helper.isAllPlanetsDownloaded(
      this.planets.length,
      this.api.totalPlanetsCount
    );
  }

  public filterPlanets(term: string, planets?: Planet[]): Planet[] {
    let planetsArray = planets;
    if (!planets) {
      planetsArray = this.planets;
    }
    return planetsArray.filter(
      val => val.name.toLowerCase().indexOf(term.toLowerCase()) !== -1
    );
  }

  private pushData(planets: Planet[], error?: string): void {
    this.planetSubject.next({
      planets,
      totalItemsCount: this.count,
      error
    });
  }

  private downloadPlanets(
    numberOfItemsToFetch: number,
    indexOfFirstItemToGet: number,
    numberOfFirstPageToFetch: number,
    searchingCb?: () => void
  ): void {
    this.api
      .fetch(numberOfItemsToFetch, numberOfFirstPageToFetch)
      .pipe(take(1))
      .subscribe(
        res => {
          if (!this.count) {
            this.count = this.api.totalPlanetsCount;
          }
          this.planets = [...this.planets, ...res];
          if (searchingCb) {
            searchingCb();
            return;
          }
          this.pushData(
            this.planets.slice(indexOfFirstItemToGet, numberOfItemsToFetch)
          );
        },
        error => {
          this.planetSubject.error('Api Error');
        }
      );
  }

  private searchPlanets(
    term: string,
    items: number,
    indexOfFirstItemToGet: number
  ): void {
    if (term && term.trim() !== '') {
      const filteredPlanets = this.filterPlanets(term, this.planets);
      this.count = filteredPlanets.length;
      if (this.count === 0) {
        this.pushData([], 'Not found');
        return;
      }
      if (items > this.count && indexOfFirstItemToGet > this.count) {
        this.pushData(filteredPlanets.slice(0, this.count));
      } else {
        if (items > this.count) {
          this.pushData(
            filteredPlanets.slice(indexOfFirstItemToGet, this.count)
          );
        } else {
          this.pushData(filteredPlanets.slice(indexOfFirstItemToGet, items));
        }
      }
    }
  }
}
