import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, tap } from 'rxjs/operators';
import { PlanetsResponse } from '../models/planets.response';
import { Observable, of } from 'rxjs';
import { Planet } from '../models/planet.interface';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { mergeMap } from 'rxjs/internal/operators/mergeMap';
import Helper from '../helpers/helper';

@Injectable()
export class ApiService {
  totalPlanetsCount: number;

  constructor(private http: HttpClient) {}

  public fetch(
    numberOfItemsToFetch: number = 10,
    firstItemIndex: number
  ): Observable<Planet[]> {
    const numberOfPagesToFetch = Helper.getNumberOfPagesToFetch(
      numberOfItemsToFetch
    );
    const lastFetchedPage = Helper.getNumberOfPagesToFetch(firstItemIndex);
    const pageToFetch = lastFetchedPage + 1;
    if (numberOfPagesToFetch === 1) {
      return this.getOnlyDataFromPlanetsPage(pageToFetch);
    } else {
      return this.getPlanetsPage(pageToFetch).pipe(
        mergeMap(firstPage => {
          return this.fetchMultiplePages(
            this.getMultiplePagesRequestArray(
              numberOfPagesToFetch,
              Helper.getNumberOfPagesToFetch(firstPage.count),
              pageToFetch + 1
            )
          ).pipe(
            map(pages => [...firstPage.results, ...pages])
          );
        })
      );
    }
  }

  public getMultiplePagesRequestArray(
    numberOfPagesToRequest: number,
    totalNumberOfPages: number,
    numberOfFirstPageToRequest: number = 0
  ): Observable<PlanetsResponse>[] {
    const requests: Observable<PlanetsResponse>[] = [];
    for (
      let i = numberOfFirstPageToRequest;
      i <= numberOfPagesToRequest && i <= totalNumberOfPages;
      i++
    ) {
      requests.push(this.getPlanetsPage(i));
    }
    return requests;
  }

  private getOnlyDataFromPlanetsPage(
    page: number = null
  ): Observable<Planet[]> {
    return this.getPlanetsPage(page).pipe(map(res => res.results));
  }

  private getPlanetsPage(page: number = null): Observable<PlanetsResponse> {
    return this.http
      .get<PlanetsResponse>(
        `https://swapi.co/api/planets${Helper.getPageNumberQueryString(page)}`
      )
      .pipe(
        tap(res => (this.totalPlanetsCount = res.count))
      );
  }

  private fetchMultiplePages(requests: Observable<PlanetsResponse>[]) {
    if (requests.length === 0) {
      return of([]);
    }
    return forkJoin(requests).pipe(
      map(res => {
        let results = [];
        res.forEach(value => (results = [...results, ...value.results]));
        return results;
      })
    );
  }

  public fetchSinglePlanet(planetId: number): Observable<Planet> {
    return this.http.get<Planet>(`https://swapi.co/api/planets/${planetId}`);
  }
}
