import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PlanetComponent } from './pages/planet/planet.component';
import { HomepageComponent } from './pages/homepage/homepage.component';
import { HttpClientModule } from '@angular/common/http';
import { ApiService } from './services/api.service';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { PageSizeComponent } from './components/page-size/page-size.component';
import { FormsModule } from '@angular/forms';
import { PlanetResolver } from './services/planet-resolver';
import { ErrorComponent } from './components/message/error.component';
import { NotFoundComponent } from './pages/not-found/not-found.component';
import { LoadingComponent } from './components/loading/loading.component';

@NgModule({
  declarations: [
    AppComponent,
    HomepageComponent,
    PlanetComponent,
    PageSizeComponent,
    ErrorComponent,
    NotFoundComponent,
    LoadingComponent
  ],
  imports: [
    FormsModule,
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    NgbModule
  ],
  providers: [ApiService, PlanetResolver],
  bootstrap: [AppComponent]
})
export class AppModule {}
